#  Tasker

This repository contains a base class for running tasks on education. It
includes several helper methods and other classes for the benefits of formatting
tasks as well as reusing functionality such as nested publishing etc.

Each project task should extend off Education\Tasker\Task. See that class for
a list of methods.

Ensure in your subclass you include `parent::run($request);` to setup the
pre-flight operations.

This module also adds support for a `schema` version number of the site. Each
site which uses this can specify a schema version and a migration task to run
on 

*mysite/_config/project.yml*
```
SilverStripe\CMS\Model\SiteTree:
  latest_schema_version: 2
  migration_class: 'UpgradeTask'
```

Now, when on `CWP` or manually running `dev/build?doCheckSchema=1` the tasker
tool will compare the current site schema version and if it is < the defined
`latest_schema_version` the `UpgradeTask` class will be run. This has been setup
for CWP so that our content migrations can be executed as part of the rollout
process.

# Cavets

This module does not handle running multiple upgrade classes to get to the 
latest state (i.e v1 to v3). You'll need to make sure jumps are major and 
rolled out sequentially or your upgrade task can handle previous versions if
needed.
