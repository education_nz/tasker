<?php

namespace Education\Tasker;

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\DataObject;
use SilverStripe\Dev\BuildTask;
use SilverStripe\Security\DefaultAdminService;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Security\IdentityStore;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Core\Convert;
use SilverStripe\Control\Cookie;
use SilverStripe\Core\Environment;
use SilverStripe\Core\ClassInfo;
use Exception;
use Page;
use Masterminds\HTML5;
use SilverStripe\Assets\File;

trait Task
{
    protected $progressCount = 0;

    protected $step = false;

    protected $verbose = false;

    protected $quiet = false;

    public function setup($request)
    {
        // increase limits
        Environment::increaseTimeLimitTo();
        Environment::increaseMemoryLimitTo();

        Cookie::config()->set('report_errors', false);

        if ($request->getVar('quiet') && $request->getVar('quiet') == 1) {
            $this->quiet = true;
        }

        if ($request->getVar('v') && $request->getVar('v') == 1) {
            $this->verbose = true;
        }

        if ($s = $request->getVar('s')) {
            $this->step = $s;
        }

        // login with admin rights so we can publish the pages
        $user = DefaultAdminService::singleton()->findOrCreateAdmin(
            'will@fullscreen.io', 'Web Services'
        );

        try {
            Injector::inst()->get(IdentityStore::class)->logIn($user);
        } catch (Exception $e) {
            //
        }

        // set stage to versioned.
        Versioned::set_stage(Versioned::DRAFT);
    }

    /**
     * @param int $step
     *
     * @return $this
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Returns a bool, based on whether we should run this step or not
     *
     */
    public function canRunStep($step)
    {
        if ($this->step) {
            if (($this->step + 1) != $step) {
                return false;
            }
        }

        return true;
    }

    public function archivePageByClass ($className)
    {
        $this->performQuery(sprintf(
            'DELETE FROM SiteTree WHERE ClassName = \'%s\'', $className
        ));

        $this->performQuery(sprintf(
            'DELETE FROM SiteTree_Live WHERE ClassName = \'%s\'', $className
        ));
    }

    public function archivePagesWithName ($name)
    {
        $pages = SiteTree::get()->filter('Title', $name);

        if ($pages) {
            foreach ($pages as $page) {
                $page->deleteFromStage('Stage');
                $page->deleteFromStage('Live');
            }
        }
    }

    /**
     * Helper function to archive a page from the live and draft stages. The
     * CMS was full of dodgy old pages which cluttered things up. The test site
     * should be used for experimenting, not production!
     *
     * @param int $pageID
     */
    public function archivePage($pageID)
    {
        $page = Versioned::get_by_stage(SiteTree::class, 'Stage', "ID = ". $pageID)->first();

        if (!$page || !$page->exists()) {
            $page = Versioned::get_by_stage(SiteTree::class, 'Live', "ID = ". $pageID)->first();
        }

        // bypass orm?
        $forceDelete = false;

        if (!$page || !$page->exists()) {
            $forceDelete = true;
        }
        else if ($page) {
            try {
                // page ondelete automatically deletes the stageChildren
                if (!$page->isOnDraftOnly()) {
                    if ($this->verbose) {
                        $this->echoMessage('Deleting #'. $page->ID . ' '. $page->Title . ' from live');
                    }

                    $page->deleteFromStage('Live');
                }

                if (!$page->isOnLiveOnly()) {
                    if ($this->verbose) {
                        $this->echoMessage('Deleting #'. $page->ID . ' '. $page->Title . ' from draft');
                    }

                    $page->deleteFromStage('Stage');
                }

                if ($this->verbose) {
                    $this->echoMessage('Deleting #'. $page->ID . ' '. $page->Title);
                }

                $page->delete();
            } catch (Exception $e) {
                $this->echoWarning('Exception archiving page, opting for force.'. $e->getMessage());

                $forceDelete = true;
            }

            $page->flushCache();
        } else {
            if ($this->verbose) {
                $this->echoWarning('Could not find SiteTree#'. $pageID . ' to archive');
            }
        }

        if ($forceDelete) {
            foreach (ClassInfo::getValidSubClasses() as $subclass) {
                $tableName = singleton($subclass)->config()->get('table_name');

                if ($this->hasTable($tableName)) {
                    $this->performQuery(sprintf('DELETE FROM %s WHERE ID = %s', $tableName, $pageID));
                    $this->performQuery(sprintf('DELETE FROM %s_Live WHERE ID = %s',$tableName, $pageID));
                }
            }
        }

        $this->echoProgress();
    }

    public function moveToRootAndDelete ($pageId)
    {
        $page = SiteTree::get()->byId($pageId);

        if ($page) {
            foreach ($page->AllChildren() as $child) {
                if ($child->isPublished()) {
                    $publish = true;
                } else {
                    $publish = false;
                }

                $child->ParentID = 0;
                $child->DisplayInMegaMenu = 0;
                $child->Sort = DB::query('SELECT MAX(Sort) FROM SiteTree WHERE ParentID = 0')->value() + 1;
                $child->write();

                if ($publish) {
                    $child->publishRecursive();
                }
            }

            $page->deleteFromStage('Live');
            $page->deleteFromStage('Stage');
        }

        $this->echoProgress();
    }

    /**
     * Helper function to change a page type to a new class name. Designed for
     * when the original class no longer exists.
     *
     */
    public function upgradePageTypes ($type, $newType)
    {
        $newType = $this->escapeNamespace($newType);
        $query = sprintf('UPDATE SiteTree SET ClassName = \'%s\' WHERE ClassName = \'%s\'', $newType,$type);
        $this->performQuery($query);

        $query = sprintf('UPDATE SiteTree_Live SET ClassName = \'%s\' WHERE ClassName = \'%s\'', $newType,$type);
        $this->performQuery($query);

        $query = sprintf('UPDATE SiteTree_Versions SET ClassName = \'%s\' WHERE ClassName = \'%s\'', $newType,$type);
        $this->performQuery($query);

        $this->echoProgress();
    }

    /**
     * Corrects a particular page class name
     *
     * @param int $id
     * @param string $newClassName
     */
    public function correctPageClass ($id, $newClassName)
    {
        $record = SiteTree::get()->byId($id);

        if ($record) {
            $updatedInstance = $record->newClassInstance($newClassName);
            $updatedInstance->write();

            if ($updatedInstance->hasMethod('publishRecursive') && $record->isPublished()) {
                $updatedInstance->publishRecursive();
            }
        }
    }

    /**
     * Returns whether a table exists
     *
     * @param string $table
     *
     * @return boolean
     */
    public function hasTable($tableName)
    {
        $tables = DB::table_list();

        return (isset($tables[strtolower($tableName)]));
    }

    /**
     * Copies the data from an old subclass table into the new class table.
     * Assumes that the underlying record (such as a page) still exists.
     *
     * @param string $oldTable
     * @param string $newClass
     * @param array $mapping
     * @param callable $callback
     */
    public function migrateTableToExistingTable ($tableName, $newClass, $mapping = [], $callback = null)
    {
        if ($this->hasTable($tableName)) {
            $data = DB::query("SELECT * FROM $tableName");

            while ($record = $data->record()) {
                $baseRecord = $newClass::get()->byId($record['ID']);

                if (!$baseRecord) {
                    // check the base table for the new class
                    if ($base = DataObject::getSchema()->baseDataClass($newClass)) {
                        if ($base != $newClass) {
                            $baseRecord = $base::get()->byId($record['ID']);
                        }
                    }

                    if (!$baseRecord) {
                        $this->echoWarning('Cannot find '. $tableName.'#'. $record['ID'] . ' to migrate to '. $newClass);

                        continue;
                    } else {
                        $baseRecord = $baseRecord->newClassInstance($newClass);
                    }
                }

                if ($baseRecord->hasMethod('publishRecursive') && $baseRecord->isPublished()) {
                    $publish = true;
                } else {
                    $publish = false;
                }

                foreach ($record as $k => $v) {
                    $d = null;

                    if (isset($mapping[$k])) {
                        if (is_array($mapping[$k])) {
                            foreach ($mapping[$k] as $dd ) {
                                $instance->{$dd} = $v;
                            }
                        } else {
                            $d = $mapping[$k];
                        }
                    } else {
                        $d = $k;
                    }

                    if ($d) {
                        $baseRecord->{$d} = $v;
                    }
                }

                if ($callback) {
                    $callback($instance);
                }

                try {
                    $baseRecord->write();

                    if ($publish) {
                        $baseRecord->publishRecursive();
                    }

                    if ($this->verbose) {
                        $this->echoMessage('Upgraded '. $tableName . '#'. $record['ID'] . ' over to '. $newClass . '#'. $baseRecord->ID);
                    } else {
                        $this->echoProgress();
                    }
                } catch (Exception $e) {
                    $this->echoWarning(sprintf(
                        'Could not write %s#%s during migration: Exception %s',
                        $tableName,
                        $baseRecord->ID,
                        $e->getMessage()
                    ));
                }
            }
        }

        $this->echoProgress();
    }

    /**
     * Helper function to use the ORM to replace the classes.
     *
     * @param string $className
     * @param string $newClass
     */
    public function upgradeInstances ($class, $newClass)
    {
        $objs = $class::get();

        foreach ($objs as $page) {
            if ($this->verbose) {
                $this->echoMessage('Upgrading '. $page->ID . ' '. $page->Title);
            }

            $updatedInstance = $page->newClassInstance($newClass);

            try {
                $updatedInstance->write();

                if ($updatedInstance->hasMethod('publishRecursive') && $page->isPublished()) {
                    $updatedInstance->publishRecursive();
                }
            } catch (Exception $e) {
                $this->echoWarning(sprintf(
                    'Could not upgrade %s#%s to %s, Exception during update: %s',
                    $class,
                    $page->ID,
                    $newClass,
                    $e->getMessage()
                ));
            }
        }

        $this->echoProgress();
    }

    /**
     * Migrates content from an abandoned table of data to a new object. For
     * instance, if a class has been deleted
     *
     * @param string $tableName
     * @param string $newClassName
     * @param array $mapping
     * @param callable $callback
     */
    public function migrateTableToObject($tableName, $newClassName, $mapping = [], $callback = null, $match = ['Title'])
    {
        $tables = DB::table_list();

        if (isset($tables[strtolower($tableName)])) {
            $data = DB::query("SELECT * FROM $tableName");

            if ($this->verbose) {
                $this->echoWarning('Upgrading '.$tableName .' into '. $newClassName);
            }

            while ($record = $data->record()) {
                $instance = $newClassName::get()->filter([
                    'ID' => $record['ID'],
                    'ClassName' => $newClassName // excludes super classes
                ])->first();

                if ($match) {
                    // validate the match.
                    foreach ($match as $column) {
                        if (!array_key_exists($column, $record)) {
                            throw new Exception($tableName .' does not have a column named '. $column);
                        }

                        if (!isset($record[$column])) {
                            throw new Exception($tableName .' does not have a column value in '. $column);
                        }

                        if (!$instance || $record[$column] !== $instance->{$column}) {
                            // check for a record with a matching title
                            $instance = $newClassName::get();

                            foreach ($match as $column) {
                                $instance = $instance->filter($column, $record[$column]);
                            }

                            $instance = $instance->first();

                            if (!$instance) {
                                $instance = $newClassName::create();

                                if ($this->verbose) {
                                    $this->echoMessage('Creating new instance of '. $newClassName);
                                }
                            }

                            break;
                        }
                    }
                }

                if (!$instance) {
                    if ($this->verbose) {
                        $this->echoMessage('Creating new instance of '. $newClassName);
                    }

                    $instance = $newClassName::create();
                } else {
                    if ($this->verbose) {
                        $this->echoMessage('Merging '. $tableName . '#'. $record['ID'] .' into existing record '. json_encode($instance));
                    }
                }

                $publish = false;

                // if we call isPublished where _Live table is not
                // present then it throws a DataException.
                try {
                    if ($instance->hasMethod('publishRecursive') && $instance->hasMethod('isPublished') && $instance->isPublished()) {
                        $publish = true;
                    } else {
                        $publish = false;
                    }
                } catch (Exception $e) {
                    //
                }

                foreach ($record as $k => $v) {
                    if ($k === 'ID') {
                        continue; // don't copy the ID for  new records.
                    }

                    $d = null;

                    if (isset($mapping[$k])) {
                        if (is_array($mapping[$k])) {
                            foreach ($mapping[$k] as $dd) {
                                $instance->{$dd} = $v;
                            }
                        } else {
                            $d = $mapping[$k];
                        }
                    } else {
                        $d = $k;
                    }

                    if ($d) {
                        $instance->{$d} = $v;
                    }
                }

                if ($callback) {
                    $callback($instance, $record);
                }

                $this->echoProgress();

                if ($instance->SkipRecord) {
                    if ($instance->isInDb()) {
                        $instance->delete();
                    }
                } else {
                    $instance->write();

                    if ($publish) {
                        $instance->publishRecursive();
                    }
                }
            }
        }

        $this->echoProgress();
    }

    /**
     * Helper function to change a dataobject type to a new class name
     *
     */
    public function upgradeClassName ($baseTable, $type, $newType)
    {
        DB::query(sprintf('UPDATE %s SET ClassName = \'%s\' WHERE ClassName = \'%s\'', $baseTable, $newType, $type));

        $this->echoProgress();
    }

    /**
     * Archive any pages that have been orphaned
     */
    public function removePagesOnLiveNotOnDraft()
    {
        $pages = DB::query('SELECT DISTINCT ID FROM SiteTree_Live WHERE (SELECT COUNT(*) FROM SiteTree WHERE SiteTree_Live.ID = SiteTree.ID) < 1 AND ParentID = 0 ORDER BY ID ASC')->column();

        if (count($pages) > 0) {
            $this->echoMessage(count($pages) . ' found on live, not on draft. Removing');

            foreach ($pages as $pageId) {
                $this->archivePage($pageId);
                $this->echoProgress();
            }
        }

        $this->echoTick();
    }

    /**
     * Fix up a URLSegment
     *
     * @param int $pageId
     * @param string $urlSegment
     *
     * @return void
     */
    public function correctURLSegment($pageId, $urlSegment)
    {
        DB::query(sprintf('UPDATE SiteTree SET URLSegment = \'%s\' WHERE ID = %s',
            $urlSegment,
            $pageId
        ))->value();

        DB::query(sprintf('UPDATE SiteTree_Live SET URLSegment = \'%s\' WHERE ID = %s',
            $urlSegment,
            $pageId
        ))->value();
    }

    /**
     * Fix up a given field
     *
     * @param int $pageId
     * @param string $fieldName
     * @param mixed $value
     *
     * @return void
     */
    public function correctFieldOnBothStages($pageId, $fieldName, $value)
    {
        DB::query(sprintf('UPDATE SiteTree SET %s = \'%s\' WHERE ID = %s',
            $fieldName,
            $urlSegment,
            $value
        ))->value();

        DB::query(sprintf('UPDATE SiteTree_Live SET %s = \'%s\' WHERE ID = %s',
            $fieldName,
            $urlSegment,
            $value
        ))->value();
    }


    /**
     * @param string $query
     */
    protected function performQuery ($query)
    {
        if ($this->verbose) {
            $this->echoMessage('Running query: '. $query);
        }

        $result = DB::query($query);

        $this->echoProgress();

        return $result;
    }

    /**
     *
     *
     */
    protected function renameColumn ($table, $oldColumn, $newColumn, $force = false)
    {
        // check to see if table exists, if it doesn't then we have already run
        // this.
        if (!$this->hasTable($table)) {
            if ($this->verbose) {
                $this->echoMessage($table . ' does not exist, skipping column rename for '. $oldColumn);
            }

            $this->echoProgress();

            return;
        }

        $result = DB::query(sprintf("SHOW COLUMNS FROM `%s` LIKE '%s'", $table, $oldColumn))->value();

        if ($result) {
            if ($force) {
                $existing = DB::query(sprintf("SHOW COLUMNS FROM `%s` LIKE '%s'", $table, $newColumn))->value();

                if ($existing) {
                    $this->performQuery(sprintf('ALTER TABLE %s DROP COLUMN `%s`', $table, $newColumn));
                }
            }


            $this->performQuery(sprintf('ALTER TABLE %s CHANGE COLUMN `%s` `%s`  VARCHAR(255)', $table, $oldColumn, $newColumn));
        }
    }

    public function updateFilePathLinks($table, $column)
    {
        $records = DB::query("SELECT ID, Content FROM $table WHERE $column LIKE '%assets/%'");

        while($record = $records->nextRecord()) {
            $id = $record['ID'];
            $content = $record['Content'];
            $html = new HTML5();
            $doc = $html->loadHTML($content);

            $needsWrite = false;

            foreach ($doc->getElementsByTagName('img') as $img) {
                // if an image links to assets and is not rewritten to use a short code then we need to add the link
                // to the short code ID if we can find it.
                $src = $img->getAttribute('src');
                $shortcode = $img->getAttribute('data-shortcode');
                $shortcodeId = $img->getAttribute('data-id');

                if (!$shortcodeId && !$shortcode && $src) {
                    if (substr($src, 0, 8) === "\/assets\/" || substr($src, 0,7) === 'assets/') {
                        if (strpos($src, '_resampled/')) {
                            $parts = explode('/', $src);

                            $file = File::get()->filter('Filename:PartialMatch:nocase', $parts[count($parts) -1])->first();
                        } else {
                            $file = File::get()->filter('Filename:PartialMatch:nocase', $src)->first();
                        }

                        // find the correct ID for the file and attach it to the image if we can.
                        if ($file) {
                            $img->setAttribute('data-shortcode', 'image');
                            $img->setAttribute('data-id', $file->ID);

                            $needsWrite = true;
                        }
                    }
                }
            }

            if ($needsWrite) {
                DB::prepared_query("UPDATE \"$table\" SET \"$column\" = ? WHERE \"ID\" = ?", [
                    $html->saveHTML($doc),
                    $id
                ]);

                $this->echoSuccess('Updated '. $id);
            }

            $this->echoProgress();
        }
    }

    /**
     * For things like invalid values in an enum you get a blank value. Not null
     * nor empty string '', so you basically can't pick it up. What this does is
     * loop over all the rows in the table and sets to a default value if it's
     * not set in PHP
     *
     * @param string $table
     * @param string $column
     * @param string $defaultValue
     */
    protected function setInvalidEnumValuesTo ($table, $column, $defaultValue)
    {
        if ($this->hasTable($table)) {
            $records = $this->performQuery("SELECT * FROM $table");

            if ($records) {
                while ($row = $records->nextRecord()) {
                    if (!$row[$column]) {
                        $this->performQuery(sprintf(
                            "UPDATE $table SET $column = '%s' WHERE ID = %s",
                            $defaultValue,
                            $row['ID']
                        ))->value();
                    }

                    $this->echoProgress();
                }
            }
        }
    }

    /**
     * Migrate data from a column on a table to another column on potentially
     * another table. Automatically handles versioned and live tables.
     *
     * Deletes the column once the data has been migrated.
     *
     * Assumes that the record ID's will match (i.e. for subclasses)
     *
     * @param string $columnFrom
     * @param string $tableFrom
     * @param string $columnTo
     * @param string $tableTo
     */
    protected function migrateDataColumnTo($columnFrom, $tableFrom, $columnTo, $tableTo)
    {
        if (!$this->hasTable($tableFrom) || !$this->hasTable($tableTo)) {
            if ($this->verbose) {
                $this->echoMessage($tableFrom. ' does not exist, skipping migrate for '. $columnFrom);
            }

            $this->echoProgress();

            return;
        }

        $result = DB::query(sprintf("SHOW COLUMNS FROM `%s` LIKE '%s'", $tableFrom, $columnFrom))->value();

        if ($result) {
            $records = DB::query(sprintf("SELECT * FROM `%s`", $tableFrom));

            while ($record = $records->nextRecord()) {
                DB::query(
                    sprintf("UPDATE %s SET %s = '%s' WHERE ID = %s",
                    $tableTo,
                    $columnTo,
                    Convert::raw2sql($record[$columnFrom]),
                    $record['ID']
                ));
            }

            // we shouldn't delete any data.
            // $this->performQuery(sprintf('ALTER TABLE %s DROP COLUMN `%s`', $tableFrom, $columnFrom));

            // check to see if Live and Versions of the table exists and if they do, then handle those.
            $tables = DB::table_list();

            if (isset($tables[$tableFrom.'_live'])) {
                $this->migrateDataColumnTo($columnFrom, $tableFrom.'_Live', $columnTo, $tableTo.'_Live');
            }

            if (isset($tables[$tableFrom.'_versions'])) {
                $this->migrateDataColumnTo($columnFrom, $tableFrom.'_Versions', $columnTo, $tableTo.'_Versions');
            }
        }
    }

    /**
     * @param string $message
     */
    protected function echoWarning ($message)
    {
        if ($this->quiet) {
            return;
        }

        echo PHP_EOL. "\033[31m [WARNING] ". $message ."\033[0m". PHP_EOL;
    }

    /**
     * @param string $heading
     */
    protected function echoHeading ($heading, $step = '.', $total = '.')
    {
        $this->progressCount = 0;

        if ($this->quiet) {
            return;
        }

        echo PHP_EOL.'+-----------------------------------------------'.PHP_EOL;
        echo "\033[32m| Step [$step/$total]:\033[0m \033[34m". $heading ."\033[0m";
        echo PHP_EOL.'+-----------------------------------------------'.PHP_EOL;
    }

    /**
     * @param string $text
     */
    protected function echoSuccess ($text)
    {
        if ($this->quiet) {
            return;
        }

        echo "\033[37m". $text ."\033[0m" .PHP_EOL.PHP_EOL;
    }

    protected function echoMessage ($text)
    {
        if ($this->quiet) {
            return;
        }

        $this->progressCount = 0;

        echo PHP_EOL. " [*] \033[31m". $text ."\033[0m" .PHP_EOL;
    }

    protected function echoTick ($message = null)
    {
        $this->progressCount = 0;

        if ($this->quiet) {
            return;
        }

        echo "\xE2\x9C\x85" .PHP_EOL;
    }

    protected function echoProgress()
    {
        if ($this->quiet) {
            return;
        }

        if ($this->progressCount > 80) {
            $this->progressCount = 0;

            echo PHP_EOL;
        }

        $this->progressCount++;

        echo '.';
    }

    protected function escapeNamespace ($class)
    {
        $class = str_replace('/', '\\', $class);
        return Convert::raw2sql($class);
    }
}
