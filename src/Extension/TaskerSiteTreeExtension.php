<?php

namespace Education\Tasker\Extension;

use SilverStripe\ORM\DB;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Controller;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Core\Environment;
use SilverStripe\Dev\Tasks\MigrateFileTask;
use Education\Tasker\JobCleanTask;

class TaskerSiteTreeExtension extends DataExtension
{
    private $run = false;

    public function requireDefaultRecords()
    {
        // script only needs to run once per dev/build.
        if ($this->run) {
            return;
        }

        $this->run = true;

        // clear out old jobs which will likely have been corrupted with any
        // database schema changes.
        $job = new JobCleanTask();
        $clean = $job->run(null);

        $state = Config::inst()->get(SiteTree::class, 'migration_on_build');

        if ($state === false) {
            DB::alteration_message('[EducationTasker] Migration on build is disabled by config flag.', 'deleted');

            return;
        } else {
            DB::alteration_message(sprintf('[EducationTasker] CWP (%s), Environment (%s)',
                (Environment::getEnv('CWP_ENVIRONMENT')) ? 'Yes' : 'No',
                Director::get_environment_type()
            ), 'created');
        }

        $always = Config::inst()->get(SiteTree::class, 'run_update_schema_always');

        if ($always || Environment::getEnv('CWP_ENVIRONMENT') || !Director::isDev() || Controller::curr()->getRequest()->getVar('doCheckSchema')) {
            // check to see if the upgrade script has run. If it has run then
            // ignore the upgrader, if it hasn't updated to the latest version
            // then trigger the update.
            //
            // Does this on dev/build since Dash will rollback the deploy if
            // accessing the homepage errors out and it usually will if the
            // schema isn't up to date.
            $currentSchema = DB::query("SELECT MAX(UpgradedSchema) FROM SiteConfig")->value();
            $latestSchema = Config::inst()->get(SiteTree::class, 'latest_schema_version');

            if (!$currentSchema || $currentSchema < $latestSchema || $always) {
                // clear caches
                SiteTree::reset();

                // run upgrade
                if ($className = Config::inst()->get(SiteTree::class, 'migration_class')) {
                    DB::alteration_message('[EducationTasker] Upgrading project to schema '. $latestSchema .'....', 'created');

                    $upgrader = Injector::inst()->create($className);

                    try {
                        $upgrader->run(new HTTPRequest('GET', '/', [
                            'quiet' => true
                        ]));

                        // update schema
                        DB::query(sprintf(
                            'UPDATE SiteConfig SET UpgradedSchema = %s',
                            $latestSchema
                        ));

                        DB::alteration_message('[EducationTasker] Upgraded project to schema '. $latestSchema, 'created');
                    } catch (Exception $e) {
                        DB::alteration_message('[EducationTasker] Error upgrading project to schema: '. $e->getMessage(), 'error');
                    }
                } else {
                   DB::alteration_message('[EducationTasker] Could not upgrade project to schema '. $latestSchema
                    .' from '. $currentSchema. '. No migration_class provided', 'deleted');
                }
            } else {
                DB::alteration_message('[EducationTasker] Project already running schema '. $latestSchema, 'created');
            }

            // trigger the image update
            if (Config::inst()->get(SiteTree::class, 'tasker_should_migrate_files')) {
                DB::alteration_message('[EducationTasker] Syncing files', 'created');

                // correct any PDF files to the
                DB::query(sprintf("
                    UPDATE File SET ClassName = '%s'
                    WHERE Filename LIKE '%s'",
                    File::class,
                    '%.pdf'
                ));

                try {
                    $task = new MigrateFileTask();

                    $task->run(new HTTPRequest('GET', '/', [
                        'quiet' => true
                    ]));
                } catch (Exception $e) {
                    DB::alteration_message('[EducationTasker] error syncing files '. $e->getMessage(), 'created');
                }
            }
        } else {
            DB::alteration_message('[EducationTasker] Migration on environment is disabled.', 'deleted');
        }
    }
}
