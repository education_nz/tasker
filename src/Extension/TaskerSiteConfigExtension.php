<?php

namespace Education\Tasker\Extension;

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;

class TaskerSiteConfigExtension extends DataExtension
{
    private static $db = [
        'UpgradedSchema' => 'Int'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->removeByName('UpgradedSchema');
    }
}
