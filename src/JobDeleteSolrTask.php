<?php

namespace Education\Tasker;

use SilverStripe\Dev\BuildTask;
use SilverStripe\ORM\DB;

class JobDeleteSolrTask extends BuildTask
{
    protected $title = 'Delete all Solr jobs';

    protected $description = "DELETE FROM QueuedJobDescriptor WHERE Implementation LIKE  '%solr%'";

    protected $enabled = true;

    private static $segment = 'JobDeleteSolrTask';

    public function run($request)
    {
        DB::query("DELETE FROM QueuedJobDescriptor WHERE Implementation LIKE  '%solr%'");

        DB::query("DELETE FROM QueuedJobDescriptor WHERE Implementation LIKE  '%search%'");
    }
}
