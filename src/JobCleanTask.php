<?php

namespace Education\Tasker;

use SilverStripe\Dev\BuildTask;
use SilverStripe\ORM\DB;

class JobCleanTask extends BuildTask
{
    protected $title = 'Job clean task';

    protected $description = 'Delete all tasks from job queue';

    protected $enabled = true;

    private static $segment = 'JobCleanTask';

    public function run($request = null)
    {
        DB::query('TRUNCATE TABLE QueuedJobDescriptor');

        echo "Done";
    }
}
